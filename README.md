# Prototipo Cansat 2023

## Inicio

Este repositorio corresponde a un prototipo realizado para el evento CANSAT Argentina del año 2023, el cual busca poder acercar a chicos de colegio secundario de todo el país (Argentina) a la experiencia espacial de llevar adelante una misión.

Para esta versión fue utilizada una placa HELTEC LORA ESP32 (V2), la cual posee tanto el ESP32, como el modulo LORA, Antena, conector de alimentación y pantalla OLED, todo incorporado.

El repositorio esta dividido en el código que se debe grabar en la placa que funciona como transmisora (que va dentro del CANSAT) como de la receptora (funcionando como estación terrena).

Vale aclarar que el evento CANSAT Argentina 2022, fue un éxito rotundo, con mas de 800 colegios inscriptos de todo el país lo cual involucra a mas de 4000 alumnos.

## Creditos

Este prototipo fue llevado adelante por las siguientes personas:
German Pagliaroli (LU6APA)
Marcelo Duca (LU1AET)
Matias Capolupo (LU1CJM)
Matias Graiño (LU4BA / ex LU9CBL)

## Contacto

Ante cualquier duda y/o sugerencia pueden contactarse al correo: lu4ba.mat[AT/ARROBA]gmail[DOT/PUNTO]com